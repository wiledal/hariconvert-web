function animate() {
  var el = arguments[0]
  var time = arguments[1]
  var from = null
  var to = arguments[2]
  var callback = arguments[4]
  var hideOnFinish = false

  el.removeEventListener('transitionend', el._onTransitionEnd)

  if (arguments[3]) {
    from = arguments[2]
    to = arguments[3]
  }

  if (from) {
    el.style.transition = 'all 0s linear'
    for (prop in from) {
      el.style[prop] = from[prop]
    }
  }

  el.offsetWidth

  if (to.display === 'none') {
    delete to.display
    hideOnFinish = true
  }

  el._onTransitionEnd = function() {
    if (hideOnFinish) el.style.display = 'none'
    if (callback) callback()
  }

  el.addEventListener('transitionend', el._onTransitionEnd)

  to.transition = 'all ' + time / 1000 + 's ease'
  for (prop in to) {
    el.style[prop] = to[prop]
  }
}

var wrapper = document.querySelector('.wrapper')
var preloader = new Image()
preloader.src = 'hariconvert-screenshot.png'
preloader.onload = function () {
  if (window.innerWidth > 680) {
    animate(wrapper, 2000, {
      opacity: 0,
      transform: 'translate(-50%, -60%) scale(.9) rotateX(30deg)'
    }, {
      opacity: 1,
      transform: 'translate(-50%, -50%) scale(1) rotateX(0)'
    }, function () {
      wrapper.setAttribute('style', 'opacity: 1;')
    })
  } else {
    animate(wrapper, 600, {
      opacity: 1
    })
  }
}

var downloadButtons = [].slice.call(document.querySelectorAll('.download'))
downloadButtons.forEach(function(button) {
  button.addEventListener('click', function () {
    if (ga) ga('send', 'event', 'Download', 'click', button.dataset.type)
  })
})
